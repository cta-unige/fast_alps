Detailed documentation
**********************

`fast_alps` configuration
==========================

Configuration is provided through TOML file.

In the ``[input]`` block you should specify the path to the data files location. 
This has to be done manually at the moment as we are not 100% sure about the data location in future.

Then you should indicate your preselection in the corresponding block. 
Use the data field names as the keys and put the arithmetic operator followed buy the magnitude of the cut as a value.
E.g. ``intensity = '>100'`` will result in selection of all events with intensity value greater then 100.

In the analysis block you should specify features of a given analysis. So far only 'wobble' and 'on_off' ones are supported.
See the supplied examples and the source code for details.

Finally, a plotting style block is provisioned, however is not yet really used and serves as a placeholder.

See below the example of configuration file with inline comments. Other configuration examples can be found under `examples` directory.

.. code-block:: python

    [input]
    data_path = '/fefs/aswg/data/real/DL2/20200127/v0.5.1_v03/'

    [preselection]
    intensity = '>300'
    leakage2_intensity = '<0.2'
    wl = '>0.01'    # 0.01
    gammaness = '>0.9'
    n_pixels = '<2000'    # 1800

    [analysis]
    type = 'on_off' # or 'wobble'
    runs_on = [1874, 1875, 1876,  1878, 1879, 1880] # for wobble analysis provide runs = [1,2,3,] array of runs to analyze
    runs_off = [1877, 1881]

    [analysis.selection]
    theta2 =  [0.075, 0.5, 2.0] # cut, normalization range min, normalization range max
    alpha = [5.0, 20.0, 80.0] # cut, normalization range min, normalization range max

    [plot_style]
    [plot_style.figure]
    figsize = 12
    [plot_style.font]
    size = 20

fast_alps usage
================
.. argparse::
    :module: fast_alps.scripts.fast_alps 
    :func: get_parser
    :prog: fast_alps


API documentation
=================

:mod:`fast_alps.core`
---------------------
.. automodule:: fast_alps.core
    :members:

:mod:`fast_alps.utils.geometry`
-------------------------------
.. automodule:: fast_alps.utils.geometry
    :members:

:mod:`fast_alps.utils.statistics`
---------------------------------
.. automodule:: fast_alps.utils.statistics
    :members:

:mod:`fast_alps.utils.plotting`
-------------------------------
.. automodule:: fast_alps.utils.plotting
    :members:

:mod:`fast_alps.utils.logger`
-----------------------------
.. automodule:: fast_alps.utils.logger
    :members:
