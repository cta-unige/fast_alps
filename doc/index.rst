Welcome to `fast_alps` documentation!
======================================

This tool aims to provide a simple to use framework to run fast preliminary analysis of the LST data.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   fast_alps
   CONTRIBUTING
   DEVELOPERS

Quick start guide
=================

Prerequisites:
--------------
    In general, you only need ``python >= 3.6``. Recommended way of installation is in virtual environment.

    Please see `development setup <DEVELOPERS.html#a-name-setup-development-setup>`_ for details.

Installation:
-------------
Clone the code from the repository::

    git clone https://gitlab.cern.ch/cta-unige/fast_alps.git && cd fast_alps

Install the code::

    flit install --user --deps=production

.. note:: 
    You may need to manually install the ``Cython`` and ``numpy`` packages to fulfill broken dependencies of ``gammapy``
    before installing ``fast_alps``.
    This issue is known to ``gammapy`` developers and will be resolved in a following release.
 
Prepare the configuration file (you can use the files under examples/ as a reference) and launch::

    fast_alps -c /path/to/configuration.toml

Running example ON/OFF analysis::

    fast_alps -c ~/doc/examples/config_on_off.toml

This command will produce the ``alpha`` and ``theta2`` plots as well as detailed log under ``/tmp/``
The plots and log are show below so you can compare.

.. image:: _static/on_off_alpha_ana.png

.. image:: _static/on_off_theta2_ana.png

.. include:: _static/fast_alps_on_off.log
   :literal:

Enjoy!

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
