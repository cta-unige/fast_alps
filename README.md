#fast_alps
Welcome to fast_alps (Fast Analysis LST Plots) package. This package allows to produce quick preliminary assessment of the LST observation data.

<!--You can find quick start guide together with detailed API documentation on the [sim_runner  website][documentation_website].-->

If you would like to contribute to the project please start with reading the [Contributor's Guide][contributors].

Enjoy!

[contributors]:CONTRIBUTING.md
[setup]:DEVELOPERS.md#setup
[documentation_website]:https://ctaunigesw-sim-runner.web.cern.ch/ctaunigesw-fast-alps/
