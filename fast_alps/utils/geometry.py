"""
Geometry utilities
"""

import astropy.units as u
import numpy as np
import pandas as pd
from astropy.coordinates import SkyCoord
from lstchain.reco.utils import radec_to_camera


def rotate(flat_object, degree=0, origin=(0, 0)):
    """
    Rotate 2D object around given axle

    :param array-like flat_object: 2D object to rotate
    :param tuple origin: rotation axle coordinates
    :param int degree: rotation angle in degrees

    :return: NDArray with new coordinates
    """
    angle = np.deg2rad(degree)
    rotation_matrix = np.array([[np.cos(angle), -np.sin(angle)],
                                [np.sin(angle), np.cos(angle)]])
    rotation_axis_coordinates = np.asarray(origin)
    res = [(rotation_matrix @ (point.T-rotation_axis_coordinates.T) +
                       rotation_axis_coordinates.T).T for point in np.atleast_2d(flat_object)]
    return res


def extract_source_position(data, observed_source_name):
    """
    Extract source position from data

    :param pandas.DataFrame data: input data
    :param str observed_source_name: Name of the observed source

    :return: 2D array of coordinates of the source in from [(x),(y)]
    """
    observed_source = SkyCoord.from_name(observed_source_name)
    obstime = pd.to_datetime(data['dragon_time'], unit='s')
    pointing_alt = u.Quantity(data['alt_tel'], u.rad, copy=False)
    pointing_az = u.Quantity(data['az_tel'], u.rad, copy=False)
    # FIXME hardcoded focal length next line
    source_pos_camera = radec_to_camera(observed_source, obstime,
                                        pointing_alt, pointing_az, focal=28*u.m)
    source_position = [source_pos_camera.x.to_value(), source_pos_camera.y.to_value()]
    return source_position
