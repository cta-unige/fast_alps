"""
Statistics utilities
"""

import numpy as np


def sigma_lima(n_on, n_off, alpha):
    """
    Implementation of Li and Ma analysis method

    :param int n_on: Number of photons for "point-on-source" observation
    :param int n_off: Number of photons for "point-off-source" observation
    :param float alpha: Ratio of observation time between "point-on-source" and "point-off-source" observations

    :return: Li&Ma significance
    """
    return np.sqrt(2 * (n_on * np.log((1+alpha)/alpha * n_on / (n_on + n_off)) +
                        n_off * np.log((1+alpha) * (n_off / (n_on + n_off)))))
