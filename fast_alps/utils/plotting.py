"""
Set of plotting utilities
"""
import matplotlib.pyplot as plt
import numpy as np
import re
from matplotlib.cm import get_cmap
from fast_alps.utils.geometry import rotate
from fast_alps.utils.logger import LOGGER


def setup(plotting_parameters):
    """
    Setup the default plotting options
    """
    plt.rcParams['figure.figsize'] = (12, 12)
    plt.rcParams['font.size'] = 20
    plt.ioff()


def plot_on_off(source_position, n_points):
    """
    Plot 2D map of ON/OFF positions w.r.t. to the camera center

    :param 2D array source_position: Position of the source
    :param int n_points: Total (on-source plus off-source) number of observation points

    """
    fig, axs = plt.subplots(1, 1, figsize=(8, 1*8))
    opacity = 0.2
    marker_size = 20
    color_map_name = 'Set1'  # https://matplotlib.org/gallery/color/colormap_reference.html
    colors = get_cmap(color_map_name).colors
    axs.set_prop_cycle(color=colors)

    rotation_angle = 360./n_points
    labels = ['Source', ] + [f'OFF {rotation_angle*(x)}' for x in range(1, n_points)]
    axs.plot((0, 0), '.', markersize=marker_size, alpha=opacity, color='black', label="Camera center")
    for _ in range(n_points):
        point = tuple(rotate(list(zip(source_position[0], source_position[1]))[0], rotation_angle * _)[0])
        axs.plot(point[0], point[1], '.', markersize=marker_size, alpha=opacity, label=labels[_])
        axs.annotate(labels[_], xy=(point[0]-0.1, point[1] + 0.05), label=labels[_])

    axs.set_ylim(-0.7, 0.7)
    axs.set_xlim(-0.7, 0.7)

    axs.set_ylabel("(m)")
    axs.set_xlabel("Position in the camera (m)")

    plot_filename = '/tmp/on_off.png'
    plt.savefig(plot_filename)
    LOGGER.info('The plot is saved under %s', plot_filename)


def plot_1d_excess(named_datasets, lima_significance,
                   x_label, x_cut, x_range_min=0, x_range_max=2,
                   n_bins=100, opacity=0.2, color_map_name='Set1'):
    """
    Plot one-dimensional distribution of signal and backgound events

    :param tuple named_datasets: A tuple of (label, data, scale factor)
    :param float lima_significance: Li&Ma significance
    :param str x_label: X-axis label
    :param float x_cut: Selection cut along X axis
    :param float x_range_min: Lower bound of X axis
    :param float x_range_max: Upper bound of X axis
    :param int n_bins: Number of bins
    :param float opacity: Histogram opacity
    :param str color_map_name: Name of the color map to use. Available maps: https://matplotlib.org/gallery/color/colormap_reference.html
    """
    fig, axs = plt.subplots(1, 1, figsize=(12, 8))
    colors = get_cmap(color_map_name).colors
    axs.set_prop_cycle(color=colors)

    hists = []
    for label, data, factor in named_datasets:
        hists.append(axs.hist(data, label=label, weights=factor*np.ones_like(data), 
                     bins=n_bins, alpha=opacity, range=[x_range_min, x_range_max]))

    axs.annotate(text=f'Significance Li&Ma = {lima_significance:.2f} $\sigma$\n',
                 xy=(np.max(hists[0][1]/4), np.max(hists[0][0]/6*5)), size=20, color='r')

    axs.vlines(x=x_cut, ymin=0, ymax=np.max(hists[0][0]*1.2), linestyle='--', linewidth=2,
               color='black', alpha=opacity)
    axs.set_xlabel(r'$\theta^2$ [deg$^2$]')
    axs.set_ylabel(r'Number of events')
    axs.legend(fontsize=12)

    plot_filename = '/tmp/' + re.sub(r'[^\w]', '', x_label) + f'_{x_cut}.png'
    plt.savefig(plot_filename)
    LOGGER.info('The plot is saved under %s', plot_filename)
